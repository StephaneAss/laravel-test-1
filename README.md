# Projet to send Applican informations

## Commands to execute
- **php artisan migrate** to fill the database
- **php artisan db:seed** to fill countries table

## configuration
- You must configure your smtp information in the .env to allow sending mail

## The url to see the interface
- **/informations**

## Autor
### Name
**Stephane ASSOCLE**

### Contacts
- **+229 67710659**
- **stephaneassocle@gmail.com**
