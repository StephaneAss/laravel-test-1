<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InformationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'firstName' => "required|string|min:3|max:255",
            'lastName' => "required|string|min:3|max:255",
            'surName' => "required|string|min:3|max:255",
            'address' => "required|string|min:3|max:255",
            'emailAddress' => "required|email",
            'phoneNumber' => "required|string|min:6|max:30",
            'region' => "required|string|min:3|max:255",
            'pincode' => "required|string|min:3|max:255",
            'country' => "required|string|min:3|max:255",
            'state' => "required|string|min:3|max:255",
            'city' => "required|string|min:3|max:255",
            'qualification' => "required|string|min:3|max:255",
            'birth_date' => "required|before:today",
            'qualification' => "required|string|min:3|max:255",
            'genderRadioOptions' => "required|string|min:3|max:255",
            'image' => "required|mimes:png,jpg,jpeg|max:2048",
            'fileResume' => "required|mimes:pdf|max:2048",
            'password' => "required|confirmed|string|min:8|max:255",
        ];
    }
}
