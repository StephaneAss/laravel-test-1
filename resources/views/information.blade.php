<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>My Informations</title>

    <!-- Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet" />
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet" />
    <!-- MDB -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.2.0/mdb.min.css" rel="stylesheet" />
</head>
<body class="bg-secondary">
    <section class="vh-100">
        <div class="container py-5 h-100">
            <div class="row justify-content-center align-items-center h-100">
                <div class="col-12 col-lg-9 col-xl-7">
                    <div class="card shadow-2-strong card-registration" style="border-radius: 15px;">
                        <div class="card-body p-4 p-md-5">
                        <h3 class="mb-4 pb-2 pb-md-0 mb-md-5">Applican's informations</h3>
                        <form action="{{route('information.save')}}" enctype="multipart/form-data" method="POST">
                            @csrf
                            <div class="row">
                                <div class="col-md-6 mb-4">
                                    <div class="form-outline">
                                        <input type="text" id="firstName" name="firstName" value="{{old('firstName')}}" class="form-control form-control-lg" />
                                        <label class="form-label" for="firstName">First Name</label>
                                    </div>
                                    <x-has_error field="firstName" />
                                </div>
                                <div class="col-md-6 mb-4">
                                    <div class="form-outline">
                                        <input type="text" id="lastName" name="lastName" value="{{old('lastName')}}" class="form-control form-control-lg" />
                                        <label class="form-label" for="lastName">Last Name</label>
                                    </div>
                                    <x-has_error field="lastName" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-4">
                                    <div class="form-outline">
                                        <input type="text" id="surName" name="surName" value="{{old('surName')}}" class="form-control form-control-lg" />
                                        <label class="form-label" for="surName">Sur Name</label>
                                    </div>
                                    <x-has_error field="surName" />
                                </div>
                                <div class="col-md-6 mb-4">
                                    <div class="form-outline">
                                        <input type="email" id="emailAddress" name="emailAddress" value="{{old('emailAddress')}}" class="form-control form-control-lg" />
                                        <label class="form-label" for="emailAddress">Email</label>
                                    </div>
                                    <x-has_error field="emailAddress" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-4">
                                    <div class="form-outline">
                                        <input type="text" class="form-control form-control-lg" value="{{old('address')}}" id="address" name="address" />
                                        <label for="address" class="form-label">Address</label>
                                    </div>  
                                    <x-has_error field="address" />
                                </div>
                                <div class="col-md-6 mb-4 pb-2">
                                    <div class="form-outline">
                                        <input type="tel" id="phoneNumber" name="phoneNumber" value="{{old('phoneNumber')}}" class="form-control form-control-lg" />
                                        <label class="form-label" for="phoneNumber">Phone Number</label>
                                    </div>
                                    <x-has_error field="phoneNumber" />
                                </div>
                                
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-4">
                                    <div class="form-outline">
                                        <input type="text" class="form-control form-control-lg" id="region" name="region" value="{{old('region')}}" />
                                        <label for="region" class="form-label">Region</label>
                                    </div>  
                                    <x-has_error field="region" />
                                </div>
                                <div class="col-md-6 mb-4 pb-2">
                                    <div class="form-outline">
                                        <input type="text" id="pincode" name="pincode" value="{{old('pincode')}}" class="form-control form-control-lg" />
                                        <label class="form-label" for="pincode">Pincode</label>
                                    </div>
                                    <x-has_error field="pincode" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-4">
                                    <div>
                                        <select name="country"  value="{{old('country')}}" class="form-control form-control-lg">
                                            <option value="">Choose country</option>
                                            @foreach ($countries as $country)
                                                <option value="{{$country->name}}">{{$country->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <x-has_error field="country" />
                                </div>
                                <div class="col-md-6 mb-4 pb-2">
                                    <div class="form-outline">
                                        <input type="text" id="state" name="state" value="{{old('state')}}" class="form-control form-control-lg" />
                                        <label class="form-label" for="state">State</label>
                                    </div>
                                    <x-has_error field="state" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-4 pb-2">
                                    <div class="form-outline">
                                        <input type="text" id="city" name="city" value="{{old('city')}}" class="form-control form-control-lg" />
                                        <label class="form-label" for="city">City</label>
                                    </div>
                                    <x-has_error field="city" />
                                </div>
                                <div class="col-md-6 mb-4 pb-2">
                                    <div class="form-outline">
                                        <input type="text" id="qualification" name="qualification" value="{{old('qualification')}}" class="form-control form-control-lg" />
                                        <label class="form-label" for="qualification">Qualification</label>
                                    </div>
                                    <x-has_error field="qualification" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-4 pb-2">
                                    <div class="form-outline">
                                        <input type="date" id="birth_date" name="birth_date" value="{{old('birth_date')}}" class="form-control form-control-lg" />
                                        <label class="form-label" for="birth_date">Birth date</label>
                                    </div>
                                    <x-has_error field="birth_date" />
                                </div>
                                <div class="col-md-6 mb-4">
                                    <h6 class="mb-2 pb-1">Gender: </h6>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="genderRadioOptions" id="femaleGender" value="Female" checked />
                                        <label class="form-check-label" for="femaleGender">Female</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="genderRadioOptions" id="maleGender" value="Male" />
                                        <label class="form-check-label" for="maleGender">Male</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="genderOptions" id="otherGender" value="Other" />
                                        <label class="form-check-label" for="otherGender">Other</label>
                                    </div>
                                    <x-has_error field="genderRadioOptions" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-4 pb-2">
                                    <div class="form-outline">
                                        <input type="file" id="image" name="image" value="{{old('image')}}" class="form-control form-control-lg" accept="image/*" />
                                        <label class="form-label" for="image">Image</label>
                                    </div>
                                    <x-has_error field="image" />
                                </div>
                                <div class="col-md-6 mb-4 pb-2">
                                    <div class="form-outline">
                                        <input type="file" id="fileResume" name="fileResume" value="{{old('fileResume')}}" class="form-control form-control-lg" accept="application/pdf" />
                                        <label class="form-label" for="fileResume">File / Resume</label>
                                    </div>
                                    <x-has_error field="fileResume" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-1 pb-2">
                                    <div class="form-outline">
                                        <input type="password" id="password" name="password" value="{{old('password')}}" class="form-control form-control-lg" accept="image/*" />
                                        <label class="form-label" for="password">Password</label>
                                    </div>
                                    <x-has_error field="password" />
                                </div>
                                <div class="col-md-6 mb-1 pb-2">
                                    <div class="form-outline">
                                        <input type="password" id="password_confirmation" name="password_confirmation" value="{{old('password_confirmation')}}" class="form-control form-control-lg" accept="application/pdf" />
                                        <label class="form-label" for="password_confirmation">Confirmation</label>
                                    </div>
                                    <x-has_error field="password_confirmation" />
                                </div>
                            </div>
                             <div class="row">
                                <div class="col-md-6">
                                    <div class="form-check">
                                        <input onclick="showPassword()" class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                        <label class="form-check-label" for="defaultCheck1">
                                          Show password
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="mt-4 pt-2">
                                <input class="btn btn-primary btn-lg" type="submit" value="Submit" />
                            </div>
            
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </section>

      <!-- MDB -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.2.0/mdb.min.js"></script>
    <script>
        function showPassword() {
            let password = document.getElementById('password');
            let password_confirmation = document.getElementById('password_confirmation');

            if (password.type == "password") {
                password.type = "text";
                password_confirmation.type = "text";
            } else {
                password.type = "password";
                password_confirmation.type = "password";
            }
        }
    </script>
</body>
</html>