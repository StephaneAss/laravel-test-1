<x-mail::message>
# Laravel developper applican's informations

<strong>First Name</strong> : {{$data->firstName}} <br>
<strong>Last Name</strong> : {{$data->lastName}} <br>
<strong>Sur Name</strong> : {{$data->surName}} <br>
<strong>Email Address</strong> : {{$data->emailAddress}} <br>
<strong>Address</strong> : {{$data->address}} <br>
<strong>Phone Number</strong> : {{$data->phoneNumber}} <br>
<strong>Region</strong> : {{$data->region}} <br>
<strong>Pincode</strong> : {{$data->pincode}} <br>
<strong>Country</strong> : {{$data->country}} <br>
<strong>State</strong> : {{$data->state}} <br>
<strong>City</strong> : {{$data->city}} <br>
<strong>Qualification</strong> : {{$data->qualification}} <br>
<strong>Birth date</strong> : {{$data->birth_date}} <br>
<strong>Gender</strong> : {{$data->genderRadioOptions}} <br>

Nearby, you can see my Resume and picture.
<br>

Thanks,<br>
{{ config('app.name') }} <br>
</x-mail::message>
